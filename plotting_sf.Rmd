---
title: "Plotting con sf"
output:
  html_document:
    depth: 4
    number_sections: yes
    theme: yeti
    toc: yes
    toc_float:
      collapsed: no
  pdf_document:
    number_sections: yes
    toc: yes
  word_document:
    toc: yes
header-includes: \renewcommand{\contentsname}{Indice}
---

```{r}
library(sf)
library(tidyverse)
```

```{r}
NIL <- st_read("data/nilzone.geojson") %>% st_transform(3857)
metro_lines <- st_read("data/tpl_metropercorsi.geojson") %>% st_transform(3857)
metro_stops <-  st_read("data/tpl_metrofermate/tpl_metrofermate.shp") %>% st_transform(3857)
```

# Plot()

### plot con attributi  

```{r}
plot(NIL)
plot(metro_lines)
plot(metro_stops)
```

### plot geometrie

```{r}
NIL %>% st_geometry() %>% plot(lwd=0.5,axes=T, col = sf.colors(22, categorical = TRUE), border = 'grey')
metro_lines %>% st_geometry() %>% plot(add=T, col="blue")
metro_stops %>% st_geometry() %>% plot(add=T, col="red")

```

# Plotting sf objects with other packages

## ggplot2

```{r}
library(ggplot2)
ggplot(NIL) + 
  geom_sf(aes(fill=AreaHA))


```

```{r}


ggplot(metro_stops) + 
  geom_sf()

```


```{r}
bike_sharing <- st_read("data/bikemi_areesosta.geojson") %>% st_transform(3857)
metro_buff <- metro_lines %>% st_buffer(dist=500)


ggplot() + 
  geom_sf(data = metro_buff, color="grey80", size=0.4, alpha=0.1)+
  geom_sf(data = metro_lines, aes(color=factor(linea)))+
  geom_sf(data = st_intersection(bike_sharing, metro_buff), color="black", alpha=0.6)
```


```{r}
library("rnaturalearth")

library("rnaturalearthdata")

world <- ne_countries(scale = "medium", returnclass = "sf")
class(world)
st_crs(world)
ggplot()+
  geom_sf(data = world)
```


```{r}
ggplot()+
  geom_sf(data = world)+
    coord_sf(crs = "+proj=laea +lat_0=52 +lon_0=10 +x_0=4321000 +y_0=3210000 +ellps=GRS80 +units=m +no_defs ")


```


## Mapview

```{r}
library(mapview)
mapview(metro_stops)
```

```{r}
mapview(metro_stops) + metro_lines
```

#tmap

```{r}
library(tmap)

tm_shape(NIL)+
  tm_polygons()

```

```{r}
tm_shape(NIL)+
  tm_polygons(col = "AreaHA")
```

```{r}
tm_shape(NIL)+
  tm_polygons(col = "AreaHA", style= "pretty")
```