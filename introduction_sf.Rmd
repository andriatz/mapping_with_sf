---
title: "Introduzione a sf"
author: "Andrea Zedda - Kode srl"
output:
  html_document:
    depth: 4
    number_sections: yes
    theme: yeti
    toc: yes
    toc_float:
      collapsed: no
  pdf_document:
    number_sections: yes
    toc: yes
  word_document:
    toc: yes
header-includes: \renewcommand{\contentsname}{Indice}
---



#Sf e SP, differenze nella struttura e nelle funzioni

## Caricamento dati 

per caricare i dati sp ed sf utilizzano delle funzioni piuttosto simili, rispettivamente **readOGR()** e **st_read()** nella disposizione dei parametri e nel riconoscimento dei formati file, st_read risulta più immediata.

```{r}
# Caricare sempre le librerie necessarie prima di iniziare

library(tidyverse)
library(sf)
library(rgdal)

sosta_sp <- rgdal::readOGR(dsn = "data/bikemi_areesosta.geojson")
sosta_sf <- sf::st_read(dsn = "data/bikemi_areesosta.geojson")
```


## Le classi degli oggetti


```{r}

class(sosta_sp)

class(sosta_sf)
```

## Le differenze nella struttura

La struttura degli oggetti sp e quelli creati da sf appare completamente diversa. Sp organizza i dati in slot, sf crea un oggetto che è sostanzialmente un data.frame con una colonna geometry che è una lista dove le informazioni geografiche sono riferite in WKT

```{r}
glimpse(sosta_sp)
glimpse(sosta_sf)
```

### La struttura di sp

```{r}
sosta_sp@data %>% head()
sosta_sp@coords %>% head()
sosta_sp@bbox
sosta_sp@proj4string

```

### La struttura di sf

```{r}
sosta_sf
st_coordinates(sosta_sf) %>% head()
st_bbox(sosta_sf)
st_crs(sosta_sf)
```

## Un primo approccio al plotting

La gunzione plot di sp ci mostra una visualizzazione delle geometrie. La medesima funzione in sf ci mostra invece anche un summary degli attributi.

```{r}
plot(sosta_sp)
plot(sosta_sf)
```

Per ottenere un risultato simile a quello di sf in sp usiamo la funziona **spplot()**
```{r}
sosta_sp %>% spplot()
```

Al contrario per plottare solo il campo delle geometrie in sf dobbiamo selezionare dolo le geometrie, per questo utilizziamo la funzione **st_geometry()**

```{r}
sosta_sf %>% st_geometry() %>% plot()
```

## Operazioni sui dati: filtrare

```{r}
sosta_sf %>%  filter(stalli>35) %>% st_geometry %>% plot()

sosta_sp[sosta_sp$stalli >35,] %>% plot
```

# Un esempio di workflow con sf


Come detto per caricare i geodati utilizziamo la funzione **st_read()**
```{r}

NIL_poly <- st_read("data/nilzone.geojson")
NIL_data <- read_csv2("data/ds205_dati_quartieri.csv") %>% select(3,2,6,8,9)


```

Una volta caricare i dati è sempre bene avere una visione d'insieme della struttura delle variabili e delle osservazioni. Le funzioni **glimpse()** e **plot()** ci aiutano ad esplorare l'aspetto degli attributi e quello delle geometrie.

```{r}
glimpse(NIL_poly)
glimpse(NIL_data)

```

```{r}
plot(NIL_poly)
```

**Summary()** è una funzione fondamentale di R base che ci permette di avere un quadro degli indici di posizione dei dati che andiamo ad esplorare

```{r}
summary(NIL_data)
```

NIL_poly contiene 88 osservazioni ovvero 88 diverse zone, mentre NIL_data contiene 621 osservazioni, perchè?

```{r}
NIL_data %>% arrange(Quartiere) %>% head()
```


Occorre sintetizzare i dati attraverso una pivot

```{r}
NIL_data %>% filter(!is.na(NIL)) %>% 
              group_by(Quartiere) %>% 
              summarise(Totale=sum(Totale),
                         Stranieri=sum(Stranieri),
                         Famiglie=sum(Famiglie)
                                               )

```

Più elegantemente possiamo sostituire summarise() con summarise_at() e ottenere il medesimo risultato

```{r}
NIL_data %>% filter(!is.na(NIL)) %>% 
  group_by(Quartiere) %>% 
  summarise_at(.vars = vars(Totale:Famiglie), .funs = sum
  )
```

Possiamo aggiungere al flow un'altra variabile, ad esempio la percentuale di stranieri
```{r}
NIL_tbl <- NIL_data %>% filter(!is.na(NIL)) %>% 
  group_by(NIL) %>% 
  summarise_at(.vars = vars(Totale:Famiglie), .funs = sum
  ) %>% 
  mutate(perc_stranieri= (Stranieri/Totale)*100) %>% 
  select(NIL, perc_stranieri)

NIL_tbl
```

A partire dai dati che abbiamo aggregato possiamo fare arricchire il nostro sf inziale, **NIL_poly**, semplicemente utilizzando la funziona **left_join()**

```{r}
 NIL_stranieri <- NIL_poly %>% 
  left_join(NIL_tbl, by=c("ID_NIL"="NIL"))

NIL_stranieri

plot(NIL_stranieri["perc_stranieri"])

```

La stessa operazione con sp sarebbe stata più complessa

```{r}

NIL_poly_sp <- NIL_poly %>% as("Spatial")

NIL_poly_sp1 <-   merge(NIL_poly_sp , NIL_tbl , by.x = "ID_NIL", by.y = "NIL")

spplot(NIL_poly_sp1["perc_stranieri"])



```

Ma soprattutto rischiosa! Un approccio più intuitivo ad esempio ci farebbe andare ad un errore molto grave

```{r}
NIL_poly_sp2 <- NIL_poly_sp

   NIL_poly_sp2@data <-  merge(NIL_poly_sp2@data, NIL_tbl, by.x = "ID_NIL", by.y = "NIL")
   
   spplot(NIL_poly_sp2["perc_stranieri"])
```

# Le principali funzioni di sf


## Plottare gli attributi o solo le geometrie

```{r}
sosta_sf  %>% st_geometry() %>% plot ()
```

```{r}
sosta_sf %>%
  st_geometry() %>%
  plot(pch=19)
```


## st_transform()

Se si lavora con più file è importante avere un crs uguale

```{r}
st_crs(NIL_poly)
st_crs(sosta_sf)
```

Se proviamo a plottare due oggetti con crs diverso abbiamo brutte sorprese

```{r}

NIL_poly %>% st_geometry() %>% plot
sosta_sf %>% st_geometry() %>% plot(add=T, col="red", pch=20)

```

**st_transform()** trasforma le coordinate in una nuova proiezione e risolve questi problemi

```{r}
NIL_poly %>% st_transform(3857) %>% st_crs()
```


```{r}
NIL_poly %>% st_transform(3857) %>%  st_geometry() %>% plot()
sosta_sf %>% st_transform(3857) %>%  st_geometry() %>% plot(add=T, col="red", pch=20)

```

## st_buffer()

Aumenta il buffer delle geometrie

```{r}
sosta_sf %>% st_geometry()%>%
             st_buffer( dist = 0.003) %>%
             plot()
```

Utilizzare un crs che usa come le unità i metri può  semplificarci il lavoro

```{r}
sosta_sf %>% st_geometry()%>%
             st_transform(3857)%>%
             st_buffer( dist = 300) %>%
             plot()
```

Ovviamente st_buffer si può applicare anche ai pologoni e il parametro **dist** può anche essere negativo

```{r}
NIL_poly %>% st_geometry() %>% st_transform(3857)%>% st_buffer(dist=300) %>% plot()
NIL_poly %>% st_geometry() %>% st_transform(3857)%>% st_buffer(dist=-300) %>% plot()
```


##st_centroid()

Calcola il centroide dei poligoni

```{r}

NIL_poly %>% st_centroid()

```


```{r}
NIL_poly %>% st_geometry() %>%
  st_transform(3857)%>%
  plot()

NIL_poly %>% st_geometry() %>% 
  st_transform(3857)%>%
  st_centroid() %>%
  plot(add=T)
```

## st_simplify()

Questa funzione ci aiuta a semplificare le geometrie
```{r}
NIL_poly %>% st_geometry() %>%
  st_transform(3857)%>%
  plot(main="Poligoni originali")

NIL_poly %>% st_geometry() %>%
  st_transform(3857)%>%
  st_simplify(dTolerance = 200 ) %>% 
  plot(main="dTolerance = 200")


NIL_poly %>% st_geometry() %>%
  st_transform(3857)%>%
  st_simplify(dTolerance = 400 ) %>% 
  plot(main="dTolerance = 400")

NIL_poly %>% st_geometry() %>%
  st_transform(3857)%>%
  st_simplify(dTolerance = 900 ) %>% 
  plot(main="dTolerance = 900")
```


## st_convex_hull()

Crea un poligono convesso a partire da dei punti

```{r}
  sosta_sf %>% 
  st_geometry() %>% plot(col=sosta_sf$zd_attuale,pch=sosta_sf$zd_attuale )
```



```{r}
sosta_sf %>%
  group_by(zd_attuale)%>% 
  summarise() %>% 
  st_geometry() %>%
  st_convex_hull() %>% 
  plot()
```

```{r}

sosta_sf %>%
  group_by(zd_attuale)%>% 
  summarise() %>% 
  st_geometry() %>%
  st_convex_hull() %>% 
  plot()

sosta_sf %>% 
  st_geometry() %>% plot(add=T, col=sosta_sf$zd_attuale,pch=sosta_sf$zd_attuale )

```


## Dissolve con st_union() e dplyr

Molte funzioni di geoprocessing come dissolve, possono essere effettuate con sf tramite funzioni proxy e le funzioni di aggregazione di dplyr

```{r}

 provs <- st_read("data/Prov2011_g/Prov2011_g_WGS84.shp") %>% st_transform(3857)

  
plot(provs)

```

```{r}

plot(provs["COD_REG"])

```

```{r}
provs %>% 
        group_by(COD_REG) %>% 
        summarise() %>% 
        plot
```

## st_join()

sf permette di eseguire agevolmente degli spatial join

```{r}

NIL_poly %>% 
    st_transform(3857) %>% 
    st_geometry() %>% 
       plot()

sosta_sf  %>% 
    st_transform(3857) %>% 
    st_geometry() %>% 
       plot(add=T, pch=16, col="red")

```

```{r}
NIL_poly <- NIL_poly %>% st_transform(3857)
sosta_sf <- sosta_sf %>% st_transform(3857)

sosta_sf_nil <- sosta_sf %>% st_join(NIL_poly)
sosta_sf_nil %>% head()
plot(sosta_sf_nil["ID_NIL"])
```

