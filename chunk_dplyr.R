data()
data("starwars")
starwars

str(starwars)
glimpse(starwars)

starwars %>% 
  # select(name:species)
  select(-c(films:starships)) %>%
  group_by(species, gender) %>% 
  summarise(n=n(),
            mass=mean(mass, na.rm = T),
            height= mean(height, na.rm = T)) %>% 
  arrange(-n) %>% 
  filter(mass<100, !is.na(species)) %>% 
  mutate(imc= mass/height) %>% 
  select(species, gender, imc) %>% 
  spread(key = gender, value = imc ) %>%  
  gather(key=gender, value=imc, -species)


NIL_data %>% group_by(Quartiere) %>%  count()

summary(NIL_data)

NIL_data %>% filter(!is.na(NIL)) %>% summary

NIL_data %>% filter(!is.na(NIL)) %>% 
                                        summarise(Totale=sum(Totale),
                                       Stranieri=sum(Stranieri),
                                       Famiglie=sum(Famiglie)
                                               )

NIL_data %>% filter(!is.na(NIL)) %>% 
  summarise_at(.vars = vars(Totale:Famiglie), .funs = sum
  )


NIL_data %>% filter(!is.na(NIL)) %>% 
  group_by(Quartiere) %>% 
  summarise_at(.vars = vars(Totale:Famiglie), .funs = sum
  )

NIL_data %>% filter(!is.na(NIL)) %>% 
  group_by(NIL) %>% 
  summarise_at(.vars = vars(Totale:Famiglie), .funs = sum
  ) %>% 
  mutate(perc_stranieri= (Stranieri/Totale)*100) %>% 
  select(NIL, perc_stranieri)
